var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const dotenv = require('dotenv');
const responseHandler = require('./controller/responseModel')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var activitiesRouter = require('./routes/activities');
var todosRouter = require('./routes/todos');
const {GeneralError} = require("./exception/exceptionHandler");

dotenv.config();
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/activity-groups', activitiesRouter);
app.use('/todo-items', todosRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    return res.status(404).json(responseHandler.notFound(null, 'Route not found!'))
});

// error handler
app.use(function(err, req, res, next) {
  if (process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'local') {
		console.log(err)
	}
	if (err instanceof GeneralError) {
		return res.status(err.getCode() || 500).json(responseHandler.generalError([err.stack], err.message))
	}
    return res.status(err.status || 500).json(responseHandler.generalError([err.message], err.message));
});

module.exports = app;
