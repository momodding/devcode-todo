const {body} = require('express-validator')

exports.createActivity = [
    body('title', 'title cannot be null').notEmpty(),
]