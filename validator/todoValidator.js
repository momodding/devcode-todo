const {body} = require('express-validator')
const models = require('../models')

exports.createTodo = [
    body('title', 'title cannot be null').notEmpty(),
    body('activity_group_id').custom(value => {
        console.log(value);
        return models.activities.findOne({
            where: {
                id: value
            }
        })
        .then(acttivity => {
            if (!acttivity) {
                return Promise.reject('Activity with activity_group_id ' + value + ' Not Found');
            }
        })
    }),
]