FROM node:16-alpine

# Create app directory
RUN mkdir -p /home/node/app
WORKDIR /home/node/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json /home/node/app

# RUN apk --no-cache add --virtual builds-deps build-base python
RUN npm install
RUN npm install pm2 -g
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . /home/node/app

EXPOSE 3030
CMD ["pm2-runtime", "bin/www"]
