var express = require('express');
var router = express.Router();
const todoController = require('../controller/todoController')
const Validator = require('../validator/todoValidator')

router.get('/', todoController.getTodos);
router.get('/:id', todoController.getTodoById);
router.post('/', Validator.createTodo, todoController.createTodo);
router.delete('/:id', todoController.deleteTodo);
router.patch('/:id', todoController.updateTodo);

module.exports = router;