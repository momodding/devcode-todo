var express = require('express');
var router = express.Router();
const activityController = require('../controller/activitiesController')
const Validator = require('../validator/activityValidator')

router.get('/', activityController.getActivities);
router.get('/:id', activityController.getActivityById);
router.post('/', Validator.createActivity, activityController.createActivity);
router.delete('/:id', activityController.deleteActivity);
router.patch('/:id',  Validator.createActivity, activityController.updateActivity);

module.exports = router;