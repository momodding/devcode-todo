exports.success = (data, message) => {
    return {
        status: "Success",
        data: data ? data : null,
        message: message ? message : 'Success'
    }
}

exports.unauthorized = (errors) => {
    return {
        status: true,
        data: errors ? errors : {},
        message: "Unauthorized. Please login first!"
    }
}

exports.notFound = (errors, message) => {
    return {
        status: true,
        data: errors ? errors : {},
        message: message ? message : "Resource not found!"
    }
}

exports.validationError = function(errors, msg) {
    return {
        status : "Bad Request",
        data : errors ? errors : {},
        message : msg ? msg : "Bad Request",
    }
}

exports.generalError = (errors, message) => {
    return {
        status: false,
        data: errors ? errors : {},
        message: message ? message : "Something went wrong. Please try again later!"
    }
}

exports.internalServerError = (errors) => {
    return {
        status: false,
        data: errors ? errors : {},
        message: "Something went wrong. Please try again later!"
    }
}
