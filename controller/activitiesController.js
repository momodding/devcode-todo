const models = require('../models')
const responseHandler = require('./responseModel')
const {GeneralError} = require("../exception/exceptionHandler");
const { validationResult } = require('express-validator')

exports.getActivities = async (req, res, next) => {
    try {
        const result = await models.activities.findAll()

        return res.status(200).json(responseHandler.success(result, "Success"))
    } catch (error) {
        next(error)
    }
}

exports.getActivityById = async (req, res, next) => {
    try {
        const { id } = req.params
        const result = await models.activities.findOne({
            where: {
                id: id
            }
        }).catch(error => {
            throw new GeneralError(null, error.message)
        })

        if (!result) {
            return res.status(404).json(responseHandler.notFound(result, "Activity with ID " + id + " Not Found"))
        }

        return res.status(200).json(responseHandler.success(result, "Success"))
    } catch (error) {
        next(error)
    }
}

exports.createActivity = async (req, res, next) => {
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            const firstError = errors.errors[0].msg
            return res.status(400).json(responseHandler.validationError(null, firstError))
        }

        const { email, title } = req.body
        const result = await models.activities.create({
            title: title,
            email: email ? email : ""
        }).catch(error => {
            throw new GeneralError(null, error.message)
        })

        return res.status(201).json(responseHandler.success(result, "Success"))
    } catch (error) {
        next(error)
    }
}

exports.deleteActivity = async (req, res, next) => {
    try {
        const { id } = req.params
        const result = await models.activities.findOne({
            where: {
                id: id
            }
        }).catch(error => {
            throw new GeneralError(null, error.message)
        })

        if (!result) {
            return res.status(404).json(responseHandler.notFound(result, "Activity with ID " + id + " Not Found"))
        }

        models.activities.destroy({
            where: {
                id: id
            }
        })

        return res.status(200).json(responseHandler.success({}, "Success"))
    } catch (error) {
        next(error)
    }
}

exports.updateActivity = async (req, res, next) => {
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            const firstError = errors.errors[0].msg
            return res.status(400).json(responseHandler.validationError(null, firstError))
        }
        
        const { id } = req.params
        const { title } =  req.body
        const result = await models.activities.findOne({
            where: {
                id: id
            }
        }).catch(error => {
            throw new GeneralError(null, error.message)
        })

        if (!result) {
            return res.status(404).json(responseHandler.notFound(result, "Activity with ID " + id + " Not Found"))
        }

        models.activities.update({
            title: title
        },
         {
            where: {
                id: id
            }
        })

        return res.status(200).json(responseHandler.success(result, "Success"))
    } catch (error) {
        next(error)
    }
}

