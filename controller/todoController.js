const models = require('../models')
const responseHandler = require('./responseModel')
const { GeneralError } = require("../exception/exceptionHandler");
const { validationResult } = require('express-validator')

exports.getTodos = async (req, res, next) => {
	try {
		const result = await models.todo.findAll()

		return res.status(200).json(responseHandler.success(result, "Success"))
	} catch (error) {
		next(error)
	}
}

exports.getTodoById = async (req, res, next) => {
	try {
		const { id } = req.params
		const result = await models.todo.findOne({
			where: {
				id: id
			}
		}).catch(error => {
			throw new GeneralError(null, error.message)
		})

		if (!result) {
			return res.status(404).json(responseHandler.notFound(result, "Todo with ID " + id + " Not Found"))
		}

		return res.status(200).json(responseHandler.success(result, "Success"))
	} catch (error) {
		next(error)
	}
}


exports.createTodo = async (req, res, next) => {
	try {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			const firstError = errors.errors[0].msg
			return res.status(400).json(responseHandler.validationError(null, firstError))
		}

		const { activity_group_id, title, is_active, priority } = req.body
		const result = await models.todo.create({
			activityGroupId: activity_group_id,
			title: title,
			isActive: is_active ? is_active : true,
			priority: priority ? priority : "very-high"
		}).catch(error => {
			throw new GeneralError(null, error.message)
		})

		return res.status(201).json(responseHandler.success(result, "Success"))
	} catch (error) {
		next(error)
	}
}

exports.deleteTodo = async (req, res, next) => {
	try {
		const { id } = req.params
		const result = await models.todo.findOne({
			where: {
				id: id
			}
		}).catch(error => {
			throw new GeneralError(null, error.message)
		})

		if (!result) {
			return res.status(404).json(responseHandler.notFound(result, "Todo with ID " + id + " Not Found"))
		}

		models.todo.destroy({
			where: {
				id: id
			}
		})

		return res.status(200).json(responseHandler.success({}, "Success"))
	} catch (error) {
		next(error)
	}
}

exports.updateTodo = async (req, res, next) => {
	try {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			const firstError = errors.errors[0].msg
			return res.status(400).json(responseHandler.validationError(null, firstError))
		}

		const { id } = req.params
		const { title, is_active, priority } = req.body
		const result = await models.todo.findOne({
			where: {
				id: id
			}
		}).catch(error => {
			throw new GeneralError(null, error.message)
		})

		if (!result) {
			return res.status(404).json(responseHandler.notFound(result, "Todo with ID " + id + " Not Found"))
		}

		models.todo.update({
			title: title,
			isActive: is_active ? is_active : result.isActive,
			priority: priority ? priority : result.priority
		},
		{
			where: {
				id: id
			}
		})

		return res.status(200).json(responseHandler.success(result, "Success"))
	} catch (error) {
		next(error)
	}
}