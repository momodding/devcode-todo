class GeneralError extends Error {
	constructor(message, stack = null) {
		super();
		this.message = message;
		this.stack = stack
	}

	getCode() {
		if (this instanceof BadRequest) {
			return 400;
		}
		if (this instanceof NotFound) {
			return 404;
		}
		if (this instanceof Unauthorized) {
			return 401;
		}
		if (this instanceof Forbidden) {
			return 403;
		}
		if (this instanceof UnprocessableEntity) {
			return 422;
		}
		return 500;
	}
}

class BadRequest extends GeneralError { }
class NotFound extends GeneralError { }
class Unauthorized extends GeneralError { }
class Forbidden extends GeneralError { }
class UnprocessableEntity extends GeneralError { }

module.exports = {
	GeneralError,
	BadRequest,
	NotFound,
	Unauthorized,
	Forbidden,
	UnprocessableEntity
};