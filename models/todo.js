'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class todo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  todo.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    activityGroupId: {
      field: 'activity_group_id',
      type: DataTypes.INTEGER
    },
    title: {
      type: DataTypes.STRING
    },
    isActive: {
      field: 'is_active',
      type: DataTypes.STRING
    },
    priority: {
      type: DataTypes.STRING
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
    deletedAt: {
      field: 'deleted_at',
      type: DataTypes.DATE,
    },
  }, {
    sequelize,
    modelName: 'todo',
    timestamps: true,
    paranoid: true,
    defaultScope: {
      where: {
        deletedAt: null
      }
    },
  });
  return todo;
};